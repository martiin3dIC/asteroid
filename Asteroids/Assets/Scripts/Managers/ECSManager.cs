using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class ECSManager : MonoBehaviour
{
    private BlobAssetStore _blobAssetStore;

    private EntityManager _manager;

    [SerializeField]
    private GameObject _playerPrefab;

    [SerializeField]
    private GameObject _playerBulletPrefab;

    [SerializeField]
    private GameObject _asteroidPrefab;

    [SerializeField]
    private GameObject _enemyPrefab;

    [SerializeField]
    private GameObject _enemyBulletPrefab;

    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private GameObject _powerUpTripleShootPrefab;

    [SerializeField]
    private GameObject _powerUpShieldPrefab;

    [SerializeField]
    private GameObject _shieldPrefab;

    private void Start()
    {
        _blobAssetStore = new BlobAssetStore();
        _manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _blobAssetStore);

        Entity explosionEntity = CreateExplosionEntity(settings);
        Entity playerBulletEntity = CreateBulletEntity(settings, _playerBulletPrefab);
        Entity enemyBulletEntity = CreateBulletEntity(settings, _enemyBulletPrefab);

        Entity playerEntity = InstantiatePlayer(settings, playerBulletEntity, explosionEntity);
        InstantiateAsteroids(settings, explosionEntity);
        StartCoroutine(InstantiateEnemy(settings, playerEntity, enemyBulletEntity, explosionEntity));

        StartCoroutine(InstantiatePowerUp(settings, playerEntity));
    }

    private Entity CreateShieldEntity(GameObjectConversionSettings settings)
    {
        Entity entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_shieldPrefab, settings);
#if UNITY_EDITOR
        _manager.SetName(entity, "Shield");
#endif
        _manager.AddComponentData(entity, new LifeTimeData { });
        _manager.AddComponentData(entity, new SetPositionData { });

        return entity;
    }

    private IEnumerator InstantiatePowerUp(GameObjectConversionSettings settings, Entity playerEntity)
    {
        Entity shieldEntity = CreateShieldEntity(settings);

        while (GameManager.instance.currentLife > 0)
        {
            yield return new WaitForSeconds(GameManager.instance.powerUpRespawnTime);
            PowerUpData.PowerUpType type = (PowerUpData.PowerUpType)UnityEngine.Random.Range(0, 2);
            Entity entity;
            if (type == PowerUpData.PowerUpType.Shield)
            {
                entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_powerUpShieldPrefab, settings);
            }
            else
            {
                entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_powerUpTripleShootPrefab, settings);
            }
#if UNITY_EDITOR
            _manager.SetName(entity, "PowerUp");
#endif
            Entity instance = _manager.Instantiate(entity);
            float posX = UnityEngine.Random.Range(GameManager.instance.screenSizeMin.x, GameManager.instance.screenSizeMax.x);
            float posZ = UnityEngine.Random.Range(GameManager.instance.screenSizeMin.y, GameManager.instance.screenSizeMax.y);

            _manager.AddComponentData(instance, new DestroyNowData { });
            _manager.AddComponentData(instance, new PowerUpData { });

            _manager.SetComponentData(instance, new DestroyNowData { });

            if (type == PowerUpData.PowerUpType.Shield)
            {
                _manager.SetComponentData(instance, new PowerUpData { type = type, playerEntity = playerEntity, timeLeft = GameManager.instance.shieldTime, effectEntity = shieldEntity });
            }
            else
            {
                _manager.SetComponentData(instance, new PowerUpData { type = type, playerEntity = playerEntity, tripleShootCount = GameManager.instance.tripleShootCount });
            }
            _manager.SetComponentData(instance, new Translation { Value = new float3(posX, 0, posZ) });
        }
    }

    private void InstantiateAsteroids(GameObjectConversionSettings settings, Entity explosionEntity)
    {
        var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_asteroidPrefab, settings);

        for (int i = 0; i < GameManager.instance.asteroidCount; i++)
        {
#if UNITY_EDITOR
            _manager.SetName(entity, "Asteroid");
#endif
            GameManager.instance.currentAsteroidsCount++;
            Entity instance = _manager.Instantiate(entity);
            float scale = UnityEngine.Random.Range(1, GameManager.instance.asteroidMaxSize);
            float3 scaleVectror = Vector3.one * scale;
            float posX = UnityEngine.Random.Range(GameManager.instance.screenSizeMin.x, GameManager.instance.screenSizeMax.x);
            float3 position = new float3(posX, 0, GameManager.instance.screenSizeMin.y);
            quaternion rotation = quaternion.Euler(0f, UnityEngine.Random.Range(0, 90), 0f);

            _manager.AddComponentData(instance, new AsteroidData { });
            _manager.AddComponentData(instance, new ScreenWrapData { });
            _manager.AddComponentData(instance, new DestroyNowData { });
            _manager.AddComponentData(instance, new NonUniformScale { });
            _manager.AddComponentData(instance, new ExplosionData { });
            _manager.AddComponentData(instance, new RespawnData { });

            _manager.SetComponentData(instance, new NonUniformScale { Value = scaleVectror });
            _manager.SetComponentData(instance, new Translation { Value = position });
            _manager.SetComponentData(instance, new Rotation { Value = rotation });
            _manager.SetComponentData(instance, new AsteroidData { speed = GameManager.instance.asteroidSpeed });
            _manager.SetComponentData(instance, new ExplosionData { explosionEntity = explosionEntity });
            _manager.SetComponentData(instance, new RespawnData { position = position, rotation = rotation, scale = scale, respawnTime = GameManager.instance.asteroidRespawnTime });
        }
    }

    private Entity CreateBulletEntity(GameObjectConversionSettings settings, GameObject prefab)
    {
        Entity entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
#if UNITY_EDITOR
        _manager.SetName(entity, "Bullet");
#endif

        _manager.AddComponentData(entity, new LifeTimeData { });
        _manager.AddComponentData(entity, new BulletData { });
        _manager.SetComponentData(entity, new LifeTimeData { timeLeft = 2 });

        return entity;
    }

    private Entity CreateExplosionEntity(GameObjectConversionSettings settings)
    {
        Entity entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_explosionPrefab, settings);
#if UNITY_EDITOR
        _manager.SetName(entity, "Explosion");
#endif

        float3 scale = Vector3.one * .2f;
        _manager.AddComponentData(entity, new LifeTimeData { });
        _manager.AddComponentData(entity, new NonUniformScale { });
        _manager.SetComponentData(entity, new LifeTimeData { timeLeft = 1.5f });
        _manager.SetComponentData(entity, new NonUniformScale { Value = scale });

        return entity;
    }

    private Entity InstantiatePlayer(GameObjectConversionSettings settings, Entity bulletEntity, Entity explosionEntity)
    {
        var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_playerPrefab, settings);
#if UNITY_EDITOR
        _manager.SetName(entity, "Player");
#endif

        Entity instance = _manager.Instantiate(entity);

        float3 position = float3.zero;
        float3 scale = new float3(.3f,.3f,.3f);
        quaternion rotation = quaternion.Euler(0f, 0f, 0f);
        float shootingCoolDown = 0.1f;

        _manager.AddComponentData(instance, new CharacterData { });
        _manager.AddComponentData(instance, new ScreenWrapData { });
        _manager.AddComponentData(instance, new NonUniformScale { });
        _manager.AddComponentData(instance, new DestroyNowData { });
        _manager.AddComponentData(instance, new ExplosionData { });
        _manager.AddComponentData(instance, new RespawnData { });
        _manager.AddComponentData(instance, new ShootingData { });

        _manager.SetComponentData(instance, new Translation { Value = position });
        _manager.SetComponentData(instance, new NonUniformScale { Value = scale });
        _manager.SetComponentData(instance, new Rotation { Value = rotation });
        _manager.SetComponentData(instance, new CharacterData { speed = GameManager.instance.playerSpeed, maxSpeed = GameManager.instance.playerMaxSpeed, rotationSpeed = GameManager.instance.playerRotationSpeed, bulletEntity = bulletEntity });
        _manager.SetComponentData(instance, new ExplosionData { explosionEntity = explosionEntity });
        _manager.SetComponentData(instance, new RespawnData { position = position, rotation = rotation, scale = scale, respawnTime = GameManager.instance.playerRespawnTime });
        _manager.SetComponentData(instance, new ShootingData { isPlayer = true, bulletSpeed = GameManager.instance.playerBulletSpeed, bulletEntity = bulletEntity, shootingTime = 0, shootingCoolDown = shootingCoolDown, maxShootCount = 1 });

        List<GameObject> bulletSpawnPoints = new List<GameObject>();
        foreach (Transform gun in _playerPrefab.transform)
        {
            if (gun.CompareTag("BulletSpawnPoint"))
            {
                bulletSpawnPoints.Add(gun.gameObject);
            }
        }

        GameManager.instance.playerGunLocation = GameManager.instance.SetGunsPositions(bulletSpawnPoints);

        return instance;
    }

    private IEnumerator InstantiateEnemy(GameObjectConversionSettings settings, Entity enemy, Entity bulletEntity, Entity explosionEntity)
    {
        yield return new WaitForSeconds(GameManager.instance.enemyRespawnTime);
        var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_enemyPrefab, settings);
#if UNITY_EDITOR
        _manager.SetName(entity, "Enemy");
#endif

        Entity instance = _manager.Instantiate(entity);
        float posX = UnityEngine.Random.Range(GameManager.instance.screenSizeMin.x, GameManager.instance.screenSizeMax.x);
        float3 position = new float3(posX, 0, GameManager.instance.screenSizeMin.y);

        float3 scale = new float3(.3f, .3f, .3f);
        quaternion rotation = quaternion.Euler(0f, 0f, 0f);
        float shootingCoolDown = GameManager.instance.enemyFireRate;

        _manager.AddComponentData(instance, new ScreenWrapData { });
        _manager.AddComponentData(instance, new NonUniformScale { });
        _manager.AddComponentData(instance, new DestroyNowData { });
        _manager.AddComponentData(instance, new ExplosionData { });
        _manager.AddComponentData(instance, new RespawnData { });
        _manager.AddComponentData(instance, new EnemyData { });
        _manager.AddComponentData(instance, new ShootingData { });

        _manager.SetComponentData(instance, new Translation { Value = position });
        _manager.SetComponentData(instance, new NonUniformScale { Value = scale });
        _manager.SetComponentData(instance, new Rotation { Value = rotation });
        _manager.SetComponentData(instance, new ExplosionData { explosionEntity = explosionEntity });
        _manager.AddComponentData(instance, new EnemyData { speed = GameManager.instance.enemySpeed, rotationSpeed = GameManager.instance.enemyRotationSpeed, playerEntity = enemy });
        _manager.SetComponentData(instance, new ShootingData { isPlayer = false, bulletSpeed = GameManager.instance.enemyBulletSpeed, bulletEntity = bulletEntity, shootingTime = shootingCoolDown / 2f , shootingCoolDown = shootingCoolDown, maxShootCount = 1 });
        _manager.SetComponentData(instance, new RespawnData { position = position, rotation = rotation, scale = scale, respawnTime = GameManager.instance.enemyRespawnTime});

        List<GameObject> bulletSpawnPoints = new List<GameObject>();
        foreach (Transform gun in _enemyPrefab.transform)
        {
            if (gun.CompareTag("BulletSpawnPoint"))
            {
                bulletSpawnPoints.Add(gun.gameObject);
            }
        }

        GameManager.instance.enemyGunLocation = GameManager.instance.SetGunsPositions(bulletSpawnPoints);
    }

    private void OnDestroy()
    {
        _blobAssetStore.Dispose();
    }
}
