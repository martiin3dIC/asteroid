using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

/// <summary>
/// RespawnSystem: It�s in charge of disable the entity (in case of the respawn flag of the RespawnData is enabled) and respawn it after x amount of time.
/// </summary>
[UpdateAfter(typeof(ExplosionSystem))]
public class RespawnSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.WithName("RespawnSystem").WithoutBurst().WithStructuralChanges().WithEntityQueryOptions(EntityQueryOptions.IncludeDisabled).
        ForEach((ref Entity entity, ref Translation position, ref Rotation rotation, ref NonUniformScale scale, ref RespawnData respawnData) =>
        {
            if (respawnData.respawn)
            {
                if (!respawnData.alreadyDisabled)
                {
                    respawnData.alreadyDisabled = true;
                    EntityManager.SetEnabled(entity, false);
                }
                respawnData.timer += deltaTime;
                if (respawnData.timer >= respawnData.respawnTime)
                {
                    respawnData.respawn = false;
                    respawnData.alreadyDisabled = false;
                    respawnData.timer = 0;
                    if (EntityManager.GetEnabled(entity) == false)
                    {
                        //Reset the explosion if we need it (just in case)
                        if (EntityManager.HasComponent<ExplosionData>(entity))
                        {
                            ExplosionData explosionData = EntityManager.GetComponentData<ExplosionData>(entity);
                            explosionData.enable = false;
                            EntityManager.SetComponentData(entity, explosionData);
                        }

                        position.Value = respawnData.position;
                        rotation.Value = respawnData.rotation;
                        scale.Value = respawnData.scale;
                        EntityManager.SetEnabled(entity, true);
                    }
                }
            }
        })
        .Run();
    }
}
