using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Collections;

/// <summary>
/// CollisionEventSystem: It�s in charge of the detection of any collision in the game, and delegate what needs to happen after that collision to the different Components Data�s.
/// </summary>
[UpdateAfter(typeof(StepPhysicsWorld)), UpdateBefore(typeof(EndFramePhysicsSystem))]
public class CollisionEventSystem : PhysicsEventSystemBase
{
    protected override void OnUpdate()
    {
        var endFramePhysicsSystem = World.GetOrCreateSystem<EndFramePhysicsSystem>();

        Dependency = new CollisionEventsJob
        {
            bulletGroup = GetComponentDataFromEntity<BulletData>(),
            physicsVelocityGroup = GetComponentDataFromEntity<PhysicsVelocity>(),
            destroyLifeTimeGroup = GetComponentDataFromEntity<LifeTimeData>(),
            asteroidGroup = GetComponentDataFromEntity<AsteroidData>(),
            characterGroup = GetComponentDataFromEntity<CharacterData>(),
            destroyNowGroup = GetComponentDataFromEntity<DestroyNowData>(),
            explosionGroup = GetComponentDataFromEntity<ExplosionData>(),
            respawnGroup = GetComponentDataFromEntity<RespawnData>(),
            enemyGroup = GetComponentDataFromEntity<EnemyData>(),
            shootGroup = GetComponentDataFromEntity<ShootingData>(),
        }.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        endFramePhysicsSystem.AddInputDependency(Dependency);
    }

    private struct CollisionEventsJob : ICollisionEventsJob
    {
        [ReadOnly] internal ComponentDataFromEntity<BulletData> bulletGroup;
        internal ComponentDataFromEntity<PhysicsVelocity> physicsVelocityGroup;
        internal ComponentDataFromEntity<LifeTimeData> destroyLifeTimeGroup;
        internal ComponentDataFromEntity<AsteroidData> asteroidGroup;
        internal ComponentDataFromEntity<CharacterData> characterGroup;
        internal ComponentDataFromEntity<DestroyNowData> destroyNowGroup;
        internal ComponentDataFromEntity<ExplosionData> explosionGroup;
        internal ComponentDataFromEntity<RespawnData> respawnGroup;
        internal ComponentDataFromEntity<EnemyData> enemyGroup;
        internal ComponentDataFromEntity<ShootingData> shootGroup;

        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.EntityA;
            Entity entityB = collisionEvent.EntityB;

            bool isBulletA = bulletGroup.HasComponent(entityA);
            bool isBulletB = bulletGroup.HasComponent(entityB);

            bool isAsteroidA = asteroidGroup.HasComponent(entityA);
            bool isAsteroidB = asteroidGroup.HasComponent(entityB);

            bool isCharacterA = characterGroup.HasComponent(entityA);
            bool isCharacterB = characterGroup.HasComponent(entityB);

            bool hasExplosionA = explosionGroup.HasComponent(entityA);
            bool hasExplosionB = explosionGroup.HasComponent(entityB);

            bool isEnemyA = enemyGroup.HasComponent(entityA);
            bool isEnemyB = enemyGroup.HasComponent(entityB);

            Entity asteroid = Entity.Null;
            Entity bullet = Entity.Null;
            Entity character = Entity.Null;
            Entity explosion = Entity.Null;
            Entity enemy = Entity.Null;

            if (isBulletA)
            {
                bullet = entityA;
            }
            else if (isAsteroidA)
            {
                asteroid = entityA;
            }
            else if (isCharacterA)
            {
                character = entityA;
            }
            else if (isEnemyA)
            {
                enemy = entityA;
            }

            if (isAsteroidB)
            {
                asteroid = entityB;
            }
            else if (isCharacterB)
            {
                character = entityB;
            }
            else if (isBulletB)
            {
                bullet = entityB;
            }
            else if (isEnemyB)
            {
                enemy = entityB;
            }

            if (hasExplosionA)
            {
                explosion = entityA;
            }
            else if (hasExplosionB)
            {
                explosion = entityB;
            }

            if(explosion != Entity.Null)
            {
                ExplosionData explosionComponent = explosionGroup[explosion];
                explosionComponent.enable = true;
                explosionGroup[explosion] = explosionComponent;
            }

            if (bullet != Entity.Null)
            {
                LifeTimeData destroyComponent = destroyLifeTimeGroup[bullet];
                destroyComponent.timeLeft = 0;
                destroyLifeTimeGroup[bullet] = destroyComponent;
            }

            // Respawn Enemy
            if(enemy != Entity.Null)
            {
                RespawnData respawnData = respawnGroup[enemy];
                if (!respawnData.respawn)
                {
                    GameManager.instance.EnemyDestroyed();
                    respawnData.respawn = true;
                    respawnGroup[enemy] = respawnData;

                    if (character != Entity.Null) // Add shoot power up to player
                    {
                        ShootingData shootData = shootGroup[character];
                        shootData.maxShootCount = 3;
                        shootGroup[character] = shootData;
                    }
                }
            }

            bool isSomethingImpactWithAnAsteroid = (asteroid != Entity.Null);

            if (isSomethingImpactWithAnAsteroid)
            {
                AsteroidData asteroidComponent = asteroidGroup[asteroid];
                asteroidComponent.destruct = true;
                asteroidGroup[asteroid] = asteroidComponent;
            }

            bool isSomethingImpactWithPlayer = (character != Entity.Null);

            if (isSomethingImpactWithPlayer)
            {
                if (GameManager.instance.currentLife > 0)
                {
                    RespawnData respawnData = respawnGroup[character];
                    if (!respawnData.respawn)
                    {
                        GameManager.instance.currentLife--;
                        respawnData.respawn = true;
                        respawnGroup[character] = respawnData;
                    }
                }
                else
                {
                    DestroyNowData destroyNowComponent = destroyNowGroup[character];
                    destroyNowComponent.destroy = true;
                    destroyNowGroup[character] = destroyNowComponent;
                }
            }
        }
    }
}