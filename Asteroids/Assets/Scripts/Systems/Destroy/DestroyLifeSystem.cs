using Unity.Entities;
using Unity.Jobs;

/// <summary>
/// DestroyLifeSystem: It�s in charge of destroy the Entity after a determined amount of seconds.
/// </summary>
[UpdateAfter(typeof(BulletMoveSystem))]
public class DestroyLifeSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.WithoutBurst().WithStructuralChanges()
        .ForEach((Entity entity, ref LifeTimeData lifetimeData) =>
        {
            lifetimeData.timeLeft -= deltaTime;
            if (lifetimeData.timeLeft <= 0f)
                EntityManager.DestroyEntity(entity);
        })
        .Run();
    }
}
