using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct CharacterData : IComponentData
{
    public float speed;
    public float maxSpeed;
    public float rotationSpeed;
    public Entity bulletEntity;
}
