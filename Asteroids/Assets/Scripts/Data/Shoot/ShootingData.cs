using Unity.Entities;

[GenerateAuthoringComponent]
public struct ShootingData : IComponentData
{
    public bool shooting;
    public bool isPlayer;
    public float bulletSpeed;
    public float shootingTime;
    public float shootingCoolDown;
    public Entity bulletEntity;
    public int maxShootCount;

    public int multiShotCount;
}
