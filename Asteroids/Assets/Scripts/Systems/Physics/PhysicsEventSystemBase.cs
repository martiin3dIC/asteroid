﻿using Unity.Entities;
using Unity.Physics.Systems;

/// <summary>
/// PhysicsEventSystemBase: Base class for all the Physics systems such as the CollisionEventSystem or the TriggerEventSystem
/// </summary>
public abstract class PhysicsEventSystemBase : SystemBase
{
    protected BuildPhysicsWorld buildPhysicsWorld;
    protected StepPhysicsWorld stepPhysicsWorld;
    protected EntityCommandBufferSystem commandBufferSystem;

    protected override void OnCreate()
    {
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
}
