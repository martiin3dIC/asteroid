using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;

/// <summary>
/// AsteroidMoveSystem: Is in charge of the asteroids movement.
/// </summary>
public class AsteroidMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        Entities.WithName("AsteroidMoveSystem").
        ForEach((ref PhysicsVelocity physics, ref Translation position, ref Rotation rotation, ref AsteroidData asteroidData) =>
        {
            physics.Angular = float3.zero;
            physics.Linear =  asteroidData.speed * math.forward(rotation.Value);
            position.Value.y = 0;

        }).ScheduleParallel();
    }
}