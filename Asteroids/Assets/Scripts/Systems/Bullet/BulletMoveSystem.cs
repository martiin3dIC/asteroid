using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Physics;

/// <summary>
/// BulletMoveSystem: Is in charge of the bullet�s movement.
/// </summary>
public class BulletMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        Entities.WithName("BulletMoveSystem").
        ForEach((ref PhysicsVelocity physics, ref Translation position, ref Rotation rotation, ref BulletData bulletData) =>
        {
            physics.Angular = float3.zero;
            physics.Linear += deltaTime * bulletData.speed * math.forward(rotation.Value);
            position.Value.y = 0;
        }).ScheduleParallel();
    }
}
