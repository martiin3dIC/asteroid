using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

/// <summary>
/// ShootingSystem: It�s in charge of spawn the bullets from an entity.
/// </summary>
[UpdateAfter(typeof(BulletMoveSystem))]
public class ShootSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;

        NativeArray<float3> playerGunPosition = new NativeArray<float3>(GameManager.instance.playerGunLocation, Allocator.TempJob);
        NativeArray<float3> enemyGunPosition = new NativeArray<float3>(GameManager.instance.enemyGunLocation, Allocator.TempJob);
        NativeArray<float3> gunPosition = new NativeArray<float3>();
        bool fire = Input.GetMouseButtonDown(0);

        Entities.WithName("ShootingSystem").WithoutBurst().WithStructuralChanges().
        ForEach((Entity entity, ref Translation position, ref Rotation rotation, ref ShootingData shootingData) =>
        {
            if (shootingData.shootingTime > 0)
            {
                shootingData.shootingTime -= deltaTime;
            }

            bool shooting = shootingData.shootingTime <= 0 && ((shootingData.isPlayer && fire) || (!shootingData.isPlayer && GameManager.instance.currentLife > 0));

            if (shooting)
            {
                shootingData.shootingTime = shootingData.shootingCoolDown;
                shootingData.shooting = false;

                if (shootingData.isPlayer)
                {
                    gunPosition = new NativeArray<float3>(playerGunPosition, Allocator.TempJob);
                }
                else
                {
                    gunPosition = new NativeArray<float3>(enemyGunPosition, Allocator.TempJob);
                }

                int count = gunPosition.Length;
                if(shootingData.maxShootCount > 1)
                {
                    if(shootingData.multiShotCount > 0)
                    {
                        shootingData.multiShotCount--;
                        if(shootingData.multiShotCount <= 0)
                        {
                            shootingData.maxShootCount = 1;
                        }
                    }
                    count = shootingData.maxShootCount;
                }
                else
                {
                    count = shootingData.maxShootCount;
                }

                for(int i = 0; i < count; i++)
                {
                    float3 gunPos = gunPosition[i];
                    var instance = EntityManager.Instantiate(shootingData.bulletEntity);
                    float3 bulletPos = position.Value + math.mul(rotation.Value, gunPos);
                    EntityManager.SetComponentData(instance, new Translation { Value = bulletPos });
                    EntityManager.SetComponentData(instance, new Rotation { Value = rotation.Value });
                    EntityManager.SetComponentData(instance, new LifeTimeData { timeLeft = 1 });
                    EntityManager.SetComponentData(instance, new BulletData { speed = 100 * count });
#if UNITY_EDITOR
                    EntityManager.SetName(instance, "Bullet");
#endif
                }

                gunPosition.Dispose();
            }
        }).Run();

        playerGunPosition.Dispose();
        enemyGunPosition.Dispose();
    }
}