using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

/// <summary>
/// AsteroidDestructionSystem: Is in charge of determining an action over the asteroid once it gets destroyed, those actions can be:
/// Rescale the asteroid and create a new instance of the asteroid(split)
/// Destroy the asteroid (because exceed the max asteroids)
/// Mark the asteroid 
/// </summary>
[UpdateAfter(typeof(ExplosionSystem))]
public class AsteroidDestructionSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float asteroidSpeed = GameManager.instance.asteroidSpeed;
        float asteroidSplittedSpeed = GameManager.instance.asteroidSplittedSpeed;

        Entities.WithName("AsteroidDestructionSystem").WithoutBurst().WithStructuralChanges().
        ForEach((Entity entity, ref RespawnData respawnData, ref Translation position, ref NonUniformScale scale,  ref Rotation rotation, ref AsteroidData asteroidData, ref DestroyNowData destroyNowData, ref ExplosionData explosionData) =>
        {
            if(asteroidData.destruct)
            {
                asteroidData.destruct = false;

                GameManager.instance.AsteroidDestroyed();
                if (scale.Value.x < 2)
                {
                    if (GameManager.instance.currentAsteroidsCount <= GameManager.instance.asteroidCount)
                    {
                        asteroidData.speed = asteroidSpeed;
                        respawnData.respawn = true;
                    }
                    else
                    {
                        GameManager.instance.currentAsteroidsCount--;
                        destroyNowData.destroy = true;
                    }
                }
                else
                {
                    scale.Value.x -= 1;
                    scale.Value.y -= 1;
                    scale.Value.z -= 1;

                    asteroidData.speed = asteroidSpeed + (asteroidSpeed/5);

                    var instance = EntityManager.Instantiate(entity);
                    GameManager.instance.currentAsteroidsCount++;

                    EntityManager.SetComponentData(instance, new NonUniformScale { Value = scale.Value });
                    EntityManager.SetComponentData(instance, new Translation { Value = position.Value });
                    EntityManager.SetComponentData(instance, new Rotation { Value = math.inverse(rotation.Value) });
                    EntityManager.SetComponentData(instance, new AsteroidData { speed = asteroidSplittedSpeed, destruct = false });
                    EntityManager.SetComponentData(instance, new ExplosionData { enable = false, explosionEntity = explosionData.explosionEntity });
#if UNITY_EDITOR
                    EntityManager.SetName(instance, "Asteroid");
#endif
                }
            }
        }).Run();
    }
}