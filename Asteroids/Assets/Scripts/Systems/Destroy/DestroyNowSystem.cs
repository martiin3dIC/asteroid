using Unity.Entities;
using Unity.Jobs;

/// <summary>
/// DestroyNowSystem: It�s in charge of destroy the Entity which has the DestroyNowData component and the destroy flag in true.
/// </summary>
[UpdateAfter(typeof(ExplosionSystem))]
public class DestroyNowSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithoutBurst().WithStructuralChanges()
        .WithName("DestroyNowSystem")
        .ForEach((Entity entity, ref DestroyNowData lifetimeData) =>
        {
            if (lifetimeData.destroy)
            {
                EntityManager.DestroyEntity(entity);
            }
        })
        .Run();
    }
}
