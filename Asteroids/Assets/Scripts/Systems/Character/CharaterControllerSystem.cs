using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Unity.Physics;

/// <summary>
/// CharacterControllerSystem: It�s in charge of the player movement.
/// </summary>
public class CharaterControllerSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float inputZ = Input.GetAxis("Vertical");
        float inputY = Input.GetAxis("Horizontal");
        float deltaTime = Time.DeltaTime;

        Entities.WithName("CharacterControllerSystem").
        ForEach((ref PhysicsVelocity physics, ref Rotation rotation, ref CharacterData characterData) =>
        {
            float translations = inputY * characterData.speed * deltaTime;

            physics.Linear += inputZ * deltaTime * characterData.speed * math.forward(rotation.Value);

            // Clamp Max Speed
            if (physics.Linear.z > characterData.maxSpeed)
            {
                physics.Linear.z = characterData.maxSpeed;
            }
            else if (physics.Linear.z < -characterData.maxSpeed)
            {
                physics.Linear.z = -characterData.maxSpeed;
            }

            if (physics.Linear.x > characterData.maxSpeed)
            {
                physics.Linear.x = characterData.maxSpeed;
            }
            else if (physics.Linear.x < -characterData.maxSpeed)
            {
                physics.Linear.x = -characterData.maxSpeed;
            }

            rotation.Value = math.mul(math.normalize(rotation.Value), quaternion.AxisAngle(math.up(), deltaTime * inputY * characterData.rotationSpeed));
        })
        .Schedule();
    }
}
