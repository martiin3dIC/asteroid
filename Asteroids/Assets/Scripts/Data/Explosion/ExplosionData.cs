using Unity.Entities;

[GenerateAuthoringComponent]
public struct ExplosionData : IComponentData
{
    public Entity explosionEntity;
    public bool enable;
}
