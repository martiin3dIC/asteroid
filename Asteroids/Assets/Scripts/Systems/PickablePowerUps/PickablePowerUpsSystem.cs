using Unity.Entities;
using Unity.Jobs;

/// <summary>
/// PickablePowerUpsSystem: It�s in charge of manage the power up which was picked (in this case create the shield or inform to the ShootData component (in the Player Entity) that the triple shoot is enabled.
/// </summary>
[UpdateBefore(typeof(DestroyNowSystem))]
public class PickablePowerUpsSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithoutBurst().WithStructuralChanges()
        .WithName("PickablePowerUpsSystem")
        .ForEach((ref PowerUpData powerUpData, ref DestroyNowData destroyNowData) =>
        {
            if (powerUpData.picked)
            {
                powerUpData.picked = false;
                destroyNowData.destroy = true;

                if (powerUpData.type == PowerUpData.PowerUpType.Shield)
                {
                    var instance = EntityManager.Instantiate(powerUpData.effectEntity);
#if UNITY_EDITOR
                    EntityManager.SetName(instance, "Shield");
#endif
                    EntityManager.AddComponentData(instance, new LifeTimeData { });
                    EntityManager.AddComponentData(instance, new SetPositionData { });

                    EntityManager.SetComponentData(instance, new LifeTimeData { timeLeft = powerUpData.timeLeft });
                    EntityManager.SetComponentData(instance, new SetPositionData { entity = powerUpData.playerEntity });
                }
                else
                {
                    if(EntityManager.Exists(powerUpData.playerEntity))
                    {
                        ShootingData shootingData = EntityManager.GetComponentData<ShootingData>(powerUpData.playerEntity);
                        shootingData.maxShootCount = 3;
                        shootingData.multiShotCount += powerUpData.tripleShootCount;
                        EntityManager.SetComponentData<ShootingData>(powerUpData.playerEntity, shootingData);
                    }
                }
            }
        })
        .Run();
    }
}
