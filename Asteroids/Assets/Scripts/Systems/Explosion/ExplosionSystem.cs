using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ExplosionSystem: It�s in charge of instantiate the explosion entity when the flag enable is set in true on the ExplosionData component.
/// </summary>
[UpdateBefore(typeof(DestroyNowSystem))]
[UpdateBefore(typeof(AsteroidDestructionSystem))]
public class ExplosionSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithoutBurst().WithStructuralChanges()
        .WithName("ExplosionSystem")
        .ForEach((ref Translation position, ref Rotation rotation, ref ExplosionData explosionData) =>
        {
            if (explosionData.enable)
            {
                explosionData.enable = false;
                var instance = EntityManager.Instantiate(explosionData.explosionEntity);
                EntityManager.SetComponentData(instance, new Translation { Value = position.Value });
                EntityManager.SetComponentData(instance, new Rotation { Value = rotation.Value });

                float3 scale = new float3(.05f, .05f,.05f);
                EntityManager.SetComponentData(instance, new NonUniformScale { Value = scale });
#if UNITY_EDITOR
                EntityManager.SetName(instance, "Explosion");
#endif
            }
        })
        .Run();
    }
}
