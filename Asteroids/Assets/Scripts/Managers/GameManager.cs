using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    #region Private Vars

    [SerializeField]
    private int _asteroidCount = 6;

    [SerializeField]
    private float _asteroidMaxSize = 4;

    [SerializeField]
    private float _asteroidSpeed = 10;

    [SerializeField]
    private float _asteroidSplittedSpeed = 13;    

    [SerializeField]
    private int _maxLife = 3;

    [SerializeField]
    private float _playerSpeed = 10;

    [SerializeField]
    private float _playerMaxSpeed = 20;

    [SerializeField]
    private float _playerRotationSpeed = 5;

    [SerializeField]
    private float _playerBulletSpeed = 100;

    [SerializeField]
    private float _playerRespawnTime = 3;

    [SerializeField]
    private float _enemyRespawnTime = 8;

    [SerializeField]
    private float _enemyBulletSpeed = 80;

    [SerializeField]
    private float _enemySpeed = 5;

    [SerializeField]
    private float _enemyFireRate = 1.5f;

    [SerializeField]
    private float _enemyRotationSpeed = 10;

    [SerializeField]
    private float _asteroidRespawnTime = 4;

    [SerializeField]
    private float _powerUpRespawnTime = 5;

    [SerializeField]
    private float _shieldTime = 7;

    [SerializeField]
    private int _tripleShootCount = 10;

    [SerializeField]
    private int _incScorePerDestroyAsteroids = 10;

    [SerializeField]
    private int _incScorePerDestroyEnemies = 15;

    [SerializeField]
    private TextMeshProUGUI _lifeTxt;

    [SerializeField]
    private TextMeshProUGUI _scoreTxt;

    private int _score;
    #endregion

    #region Public Vars
    [HideInInspector]
    public int currentLife;

    [HideInInspector]
    public int currentAsteroidsCount;
    #endregion

    #region Properties
    public float2 screenSizeMin { get; private set; }
    public float2 screenSizeMax { get; private set; }
    public float3[] playerGunLocation { get; set; }
    public float3[] enemyGunLocation { get; set; }
    public int asteroidCount { get => _asteroidCount;  }
    public float asteroidMaxSize { get => _asteroidMaxSize;  }
    public float asteroidSpeed { get => _asteroidSpeed; }
    public float asteroidSplittedSpeed { get => _asteroidSplittedSpeed; }
    public int maxLife { get => _maxLife; }
    public float playerSpeed { get => _playerSpeed;  }
    public float playerMaxSpeed { get => _playerMaxSpeed;  }
    public float playerRotationSpeed { get => _playerRotationSpeed;  }
    public float playerBulletSpeed { get => _playerBulletSpeed; }
    public float playerRespawnTime { get => _playerRespawnTime;  }
    public float enemyRespawnTime { get => _enemyRespawnTime; }
    public float enemyBulletSpeed { get => _enemyBulletSpeed; }
    public float enemyFireRate { get => _enemyFireRate; }
    public float enemySpeed { get => _enemySpeed;  }
    public float enemyRotationSpeed { get => _enemyRotationSpeed; }
    public float asteroidRespawnTime { get => _asteroidRespawnTime; }
    public float powerUpRespawnTime { get => _powerUpRespawnTime; }
    public float shieldTime { get => _shieldTime; }
    public int tripleShootCount { get => _tripleShootCount; }
    #endregion

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;

        Vector3 screen = new Vector3(Screen.width, Screen.height, Camera.main.transform.position.y);
        Vector3 world = Camera.main.ScreenToWorldPoint(screen);

        screenSizeMin = new float2(world.x * -1, world.z * -1);
        screenSizeMax = new float2(world.x, world.z);

        currentLife = maxLife;
        _lifeTxt.text = currentLife.ToString();
        playerGunLocation = new float3[0];
        enemyGunLocation = new float3[0];
    }

    public float3[] SetGunsPositions(List<GameObject> bulletSpawnPoints)
    {
        float3[] gunLocation = new float3[bulletSpawnPoints.Count];

        for (int i = 0; i < bulletSpawnPoints.Count; i++)
        {
            gunLocation[i] = bulletSpawnPoints[i].transform.localPosition * 0.3f;
        }

        return gunLocation;
    }

    public void AsteroidDestroyed()
    {
        _score += _incScorePerDestroyAsteroids;
    }

    public void EnemyDestroyed()
    {
        _score += _incScorePerDestroyEnemies;
    }

    private void LateUpdate()
    {
        _lifeTxt.text = currentLife.ToString();
        _scoreTxt.text = _score.ToString();
    }
}
