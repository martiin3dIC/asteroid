using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct RespawnData : IComponentData
{
    public bool respawn;
    public float3 position;
    public quaternion rotation;
    public float3 scale;
    public float respawnTime;
    public float timer;
    public bool alreadyDisabled;
}
