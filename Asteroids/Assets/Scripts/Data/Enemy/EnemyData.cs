using Unity.Entities;

[GenerateAuthoringComponent]
public struct EnemyData : IComponentData
{
    public float speed;
    public float rotationSpeed;
    public Entity playerEntity;
}
