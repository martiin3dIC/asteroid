using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

/// <summary>
/// SetPositionSystem: It�s in charge of set entity position as the same position of another entity.
/// </summary>
public class SetPositionSystem : SystemBase
{
    protected override void OnUpdate()
    {
        bool hasPosition = false;
        bool isEnabled = false;
        float3 targetPosition = float3.zero;

        // Doing it like this to get just once the player position and enable to run the chase functionality in parallel
        Entities.WithName("GeTarget").WithoutBurst()
        .ForEach((SetPositionData setPositionData) =>
        {
            if (!hasPosition)
            {
                if (EntityManager.Exists(setPositionData.entity))
                {
                    hasPosition = true;
                    isEnabled = EntityManager.GetEnabled(setPositionData.entity);
                    Translation position = EntityManager.GetComponentData<Translation>(setPositionData.entity);
                    targetPosition = position.Value;
                }
            }
        }).Run();

        Entities.WithName("SetPositionSystem").
        ForEach((ref Translation position, ref SetPositionData setPositionData, ref LifeTimeData lifeTimeData) =>
        {
            if (hasPosition && isEnabled)
            {
                position.Value = targetPosition;
            }
            else
            {
                lifeTimeData.timeLeft = 0;
            }
        }).ScheduleParallel();
    }
}