using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

/// <summary>
/// TriggerEventSystem: It�s in charge of the detection of any physic trigger in the game, and delegate what needs to happen after that trigger to the different Components Data�s.
/// </summary>
public class TriggerEventSystem : PhysicsEventSystemBase
{
    protected override void OnUpdate()
    {
        var endFramePhysicsSystem = World.GetOrCreateSystem<EndFramePhysicsSystem>();

        Dependency = new TriggerEventsJob
        {
            physicsVelocityGroup = GetComponentDataFromEntity<PhysicsVelocity>(),
            characterGroup = GetComponentDataFromEntity<CharacterData>(),
            powerUpGroup = GetComponentDataFromEntity<PowerUpData>(),
        }.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        endFramePhysicsSystem.AddInputDependency(Dependency);
    }

    private struct TriggerEventsJob : ITriggerEventsJob
    {
        internal ComponentDataFromEntity<CharacterData> characterGroup;
        internal ComponentDataFromEntity<PhysicsVelocity> physicsVelocityGroup;
        internal ComponentDataFromEntity<PowerUpData> powerUpGroup;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity entityA = triggerEvent.EntityA;
            Entity entityB = triggerEvent.EntityB;

            bool isCharacterA = characterGroup.HasComponent(entityA);
            bool isCharacterB = characterGroup.HasComponent(entityB);

            bool isPowerUpA = powerUpGroup.HasComponent(entityA);
            bool isPowerUpB = powerUpGroup.HasComponent(entityB);

            Entity powerUp = Entity.Null;
            Entity character = Entity.Null;

            if (isPowerUpA)
            {
                powerUp = entityA;
            }
            else if (isCharacterA)
            {
                character = entityA;
            }

            if (isPowerUpB)
            {
                powerUp = entityB;
            }
            else if (isCharacterB)
            {
                character = entityB;
            }

            if (powerUp != Entity.Null && character != Entity.Null)
            {
                //PowerUp pick
                PowerUpData powerUpComponent = powerUpGroup[powerUp];
                if (!powerUpComponent.picked)
                {
                    powerUpComponent.picked = true;
                    powerUpGroup[powerUp] = powerUpComponent;
                }
            }
        }
    }
}