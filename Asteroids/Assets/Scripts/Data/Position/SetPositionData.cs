using Unity.Entities;

[GenerateAuthoringComponent]
public struct SetPositionData : IComponentData
{
    public Entity entity;
}
