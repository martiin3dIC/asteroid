using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerUpData : IComponentData
{
    public enum PowerUpType
    {
        Shield,
        TripleShoot,
    }
    public bool picked;
    public PowerUpType type;
    public Entity effectEntity;
    public Entity playerEntity;
    public float timeLeft;
    public int tripleShootCount;
}
