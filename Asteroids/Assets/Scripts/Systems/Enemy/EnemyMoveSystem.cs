using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;

/// <summary>
/// EnemyMoveSystem: It�s in charge of the enemy spaceship movements (Chase the player)
/// </summary>
public class EnemyMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        bool hasEnemyPosition = false;
        bool lookAt = false;
        float3 targetPosition = float3.zero;

        // Doing it like this to get just once the player position and enable to run the chase functionality in parallel
        Entities.WithName("GetEnemyTarget").WithoutBurst()
        .ForEach((EnemyData enemyData) =>
        {
            if(!hasEnemyPosition)
            {
                if (EntityManager.Exists(enemyData.playerEntity))
                {
                    lookAt = EntityManager.GetEnabled(enemyData.playerEntity);
                    hasEnemyPosition = true;
                    Translation position = EntityManager.GetComponentData<Translation>(enemyData.playerEntity);
                    targetPosition = position.Value;
                }
            }
        }).Run();

        Entities.WithName("EnemyMoveSystem").
        ForEach((ref PhysicsVelocity physics, ref Translation position, ref Rotation rotation, ref EnemyData enemyData) =>
        {
            if (hasEnemyPosition)
            {
                float3 heading = targetPosition - position.Value;
                if (lookAt)
                {
                    quaternion targetDirection = quaternion.LookRotation(heading, math.up());
                    rotation.Value = math.slerp(rotation.Value, targetDirection, enemyData.rotationSpeed * deltaTime);
                }
                else
                {
                    physics.Angular = float3.zero;
                }
                position.Value += deltaTime * enemyData.speed * math.forward(rotation.Value);
            }
            else
            {
                physics.Angular = float3.zero;
                physics.Linear = 0;
            }
            position.Value.y = 0;

        }).ScheduleParallel();
    }
}