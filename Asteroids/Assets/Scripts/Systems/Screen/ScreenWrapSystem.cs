using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

/// <summary>
/// ScreenWrapSystem: It�s in charge of set the entity position to their opposite if the entity is out of screen.
/// </summary>
public class ScreenWrapSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float2 screenSizeMin = new float2(GameManager.instance.screenSizeMin);
        float2 screenSizeMax = new float2(GameManager.instance.screenSizeMax);

        Entities.WithName("ScreenWrapSystem").
        ForEach((ref Translation position, ref NonUniformScale scale, ref ScreenWrapData screenWrapData) =>
        {
            if (position.Value.x + scale.Value.x < screenSizeMin.x || position.Value.x - scale.Value.x > screenSizeMax.x)
            {

                if (position.Value.x - scale.Value.x > screenSizeMax.x)
                {
                    position.Value.x -= scale.Value.x;
                }
                else if (position.Value.x + scale.Value.x < screenSizeMin.x)
                {
                    position.Value.x += scale.Value.x;
                }

                //Wrap position
                position.Value.x *= -1;
            }
            else if (position.Value.z + scale.Value.z < screenSizeMin.y || position.Value.z - scale.Value.z > screenSizeMax.y)
            {
                if (position.Value.z - scale.Value.z > screenSizeMax.y)
                {
                    position.Value.z -= scale.Value.z;
                }
                else if (position.Value.z + scale.Value.z < screenSizeMin.y)
                {
                    position.Value.z += scale.Value.z;
                }

                //Wrap position
                position.Value.z *= -1;
            }
        }).Schedule();
    }
}
